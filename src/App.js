//Modules imports
import React from 'react'
//Component imports
import IntroExperience from './pages/intro.js'
import EnterParticipant from './pages/intro-enter.js'
import TakePhoto from './pages/intro-photo.js'
import HallofFame from './pages/halloffame.js'
import Badge from './pages/Badge.js'
import Video2 from './pages/video-2.js'
import ChooseCareer from './pages/career.js'
import Transition1 from './pages/transition1.js'
import Deepfake1 from './pages/deepfake1.js'
import BlackHole from './pages/balckholevideo.js'
import Deepfake2 from './pages/deepfake2.js'
import Transition2 from './pages/transition2.js'
import Outro from './pages/outro.js'

// Create App class
class App extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            // API Link
            api:"https://photobooth-api.cfapps.eu10.hana.ondemand.com",
            api2: "https://photobooth-api.cfapps.eu10.hana.ondemand.com",
            currentPage: 0,
            // User's data
            participant: Date.now().toString(),
            participantName: "",
            participantGender: "",
            participantPhoto: "",
            participantEmail: "",
            participantCareer: "",
            //Deepfake photos
            deepfake1: "",
            deepfake2: "",
        }
        this.nextPage = this.nextPage.bind(this)
        this.restartApp = this.restartApp.bind(this)
        this.inputData = this.inputData.bind(this)
        this.inputCareer = this.inputCareer.bind(this)
        this.inputEmail = this.inputEmail.bind(this)
        this.inputPhoto = this.inputPhoto.bind(this)
        this.inputDeepfake = this.inputDeepfake.bind(this)
    }

    nextPage = () => {
        // Function to go to the next page 
        const {currentPage} = this.state;
        console.log(currentPage)
        this.setState({
            currentPage: currentPage + 1
        })

    }

    restartApp = () => {
        // function to restart the app
        window.location.reload()
    }

    inputData = (name, gender) => {
        //Function that get Name and Gender of the user 
        this.setState({participantName: name})
        this.setState({participantGender: gender})
    }
    inputEmail = (email) => {
        // Get the user's email
        this.setState({participantEmail: email})
    }
    inputCareer = (Career) => {
        // Get the user's career choice 
        this.setState({participantCareer: Career})
        console.log(this.state.participantCareer)
    }
    inputPhoto = (photo) => {
        // Get the user's taken photo
        this.setState({participantPhoto: photo})
        console.log('this the photo')
        console.log(this.state.participantPhoto)
    }
    inputDeepfake = (photo1, photo2) => {
        // Preprocess and record the deepfake photos
        let data = photo1.slice(2)
        let data2 = photo2.slice(2)
        let dataf = data.slice(0,-1)
        let dataf2 = data2.slice(0,-1)
        this.setState({deepfake1: dataf})
        this.setState({deepfake2: dataf2})
        console.log(this.state.deepfake1)
    }
// Rendering the application 
// Logic is simple : you put the page given the page with just the synthax case 
    render(){
        // Background style to apply to the different pages 
        // For the User's data form 
        const myStyleForm={
            backgroundImage:`url(${process.env.PUBLIC_URL+ "/check-in-background-02.jpg"})`,
                    height:'100vh',
                    backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat',
                    };
        // Basic app background
        const myStyle={
            backgroundImage:`url(${process.env.PUBLIC_URL+ "/Time-travel-intro.jpg"})`,
                    height:'100vh',
                    backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat',
                    };
        // Career page background 
        const CareerStyle={
            backgroundImage:`url(${process.env.PUBLIC_URL+ "/choose-career-options.jpg"})`,
                    height:'100vh',
                    backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat',
                    };
        switch (this.state.currentPage){
            case 0:
            // Intro video page 
            return (
                <div className='app'>
                   <IntroExperience
                        participant = {this.state.participant}
                        pageHandler = {this.nextPage}
                        dataHandler = {this.inputData}
                    />
                </div>  
            )
            case 1:
            return (
                <div className='app' style={myStyleForm}>
                      <EnterParticipant
                        participant = {this.state.participant}
                        pageHandler = {this.nextPage}
                        dataHandler = {this.inputData}
                        emailHandler = {this.inputEmail}
                    />
                </div>  
            )
            case 2:
            return (
                <div className='app applybackgroundcolor' >
                    <TakePhoto
                        participant = {this.state.participant}
                        gender = {this.state.participantGender}
                        pageHandler = {this.nextPage}
                        api = {this.state.api2}
                        participantPhoto = {this.state.participantPhoto}
                        dataHandler = {this.inputPhoto}
                    />
                </div>
            )
            case 3:
            return (
                <div className='app' style={myStyle}>
                    <img className='logo' src="logo.svg" alt="SAP logo"/>
                    <HallofFame
                        participant = {this.state.participant}
                        pageHandler = {this.nextPage}
                        dataHandler = {this.inputData}
                    /> 

                </div>
            )
            case 4:
            return (
                <div className='app'  style={myStyle}>
                    <img className='logo' src="logo.svg" alt="SAP logo"/>
                    <Badge
                        participant = {this.state.participant}
                        pageHandler = {this.nextPage}
                        dataHandler = {this.inputData}
                        photo = {this.state.participantPhoto}
                    />
                </div>
            )
            case 5:
            return (
                <div className='app' >
                    <img className='logo' src="logo.svg" alt="SAP logo"/>
                    <Video2
                        gender = {this.state.participantGender}
                        participant = {this.state.participant}
                        pageHandler = {this.nextPage}
                        dataHandler = {this.inputData}
                        dfHandler = {this.inputDeepfake}
                        image = {this.state.participantPhoto}
                        api = {this.state.api}
                        photo1 = {this.state.deepfake1}
                        photo2 = {this.state.deepfake2}
                    />
                </div>
            )
            case 6:
            return (
                <div className='app' style={CareerStyle}>
                    <ChooseCareer
                        participant = {this.state.participant}
                        pageHandler = {this.nextPage}
                        dataHandler = {this.inputCareer}
                        api = {this.state.api}
                        name = {this.state.participantName}
                        mail = {this.state.participantEmail}
                    />
                
                </div>
            )
            case 7 :
                return (
                    <div className='app'>
                    <img className='logo' src="logo.svg" alt="SAP logo"/>
                    <Transition1
                        participant = {this.state.participant}
                        pageHandler = {this.nextPage}
                        dataHandler = {this.inputData}
                    />
                    
                </div>
                )
            case 8:
                return (
                    <div className='app' style={myStyle}>
                        <img className='logo' src="logo.svg" alt="SAP logo"/>
                    <Deepfake1
                        participant = {this.state.participant}
                        Career = {this.state.participantCareer}
                        pageHandler = {this.nextPage}
                        dataHandler = {this.inputData}
                        photo1 = {this.state.deepfake1}

                    />
                    
                    </div>
                )
          
            
            case 9 :
                return (
                    <div className='app'>
                    <img className='logo' src="logo.svg" alt="SAP logo"/>
                    <BlackHole
                        participant = {this.state.participant}
                        pageHandler = {this.nextPage}
                        dataHandler = {this.inputData}
                    />
                  
                </div>
                )
            case 10:
            return (
                <div className='app' style={myStyle}>
                    <img className='logo' src="logo.svg" alt="SAP logo"/>
                    <Deepfake2
                        participant = {this.state.participant}
                        Career = {this.state.participantCareer}
                        pageHandler = {this.nextPage}
                        dataHandler = {this.inputData}
                        photo2 = {this.state.deepfake2}
                    />
                    
                </div>
            )
            case 11:
                return (
                    <div className='app'>
                        <img className='logo' src="logo.svg" alt="SAP logo"/>
                        <Transition2
                        participant = {this.state.participant}
                        pageHandler = {this.nextPage}
                        dataHandler = {this.inputData}
                        restartApp = {this.restartApp}
                    />
                    </div>
                )
            case 12 :
                // Additional page to wait for the reload 
                return (
                    <div className='app'>
                    <img className='logo' src="logo.svg" alt="SAP logo"/>
                    <Outro
                        participant = {this.state.participant}
                        pageHandler = {this.nextPage}
                        dataHandler = {this.inputData}
                    />
                  
                    
                </div>
                )
        }
    }
}

export default App
