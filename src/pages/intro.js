import React from 'react';
import ReactPlayer from 'react-player';
import {Input, Button, Dropdown, Modal} from '@kerberos-io/ui';

class IntroExperience extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            // Variable to interact btw Gif and video
            showVideo : false 
        }
        this.TimeoutVideo = this.TimeoutVideo.bind(this);

    }
    TimeoutVideo = (event) =>  {
        // Function to automatically go to the next page at the end of the video and manage interaction btw gif and video
        this.setState({showVideo: true})
        setTimeout(() => {
            this.props.pageHandler();
        }, 42000);
    }
    render(){
        return (
            <div className='question-section'>
                <div className="rendering">
                {!this.state.showVideo && <img src='touch-screen-start.gif' onClick={this.TimeoutVideo}/>}
                {this.state.showVideo && <video id={"introVideo"} className="introVideo" width={"100%"} autoPlay>
                        <source src="00_INTRO.mp4" type="video/mp4"/>
                    </video> }
                </div>
            </div>
        )
    }
}

export default IntroExperience
