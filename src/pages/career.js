import React from 'react';
import IT from '../Media/IT.jpg'
import HR from '../Media/HR.jpg'
import './careerpaths.css'
import ReactAudioPlayer from 'react-audio-player'; 
import Marketing from '../Media/marketing.jpg'
import Finance from '../Media/finance.jpg'

class ChooseCareer extends React.Component {

    constructor(props) {
        super(props);
        this.state =  {
            Career: ""
        }
        

        this.handleCareer = this.handleCareer.bind(this);
    }

    sendMail = (career) => {
        // This function send an email but also registered the mail and career into Mongo DB
        // Send mail
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
        "mail": this.props.mail,
        "career": career,
        "username": this.props.name
        });

        var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
        };

        fetch( this.props.api + "/email_newsletter", requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));

        // DB 
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
        "mail": this.props.mail,
        "career": career
        });

        var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
        };

        fetch(this.props.api + "/db/email", requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));
    }

    handleCareer = async (event) => {
        // Get the user's career choice 
        await this.setState({Career: event.target.value})
        this.props.dataHandler(this.state.Career)
        this.props.pageHandler()
        if(this.props.mail != ""){
            this.sendMail(this.state.Career);
        }
    }

    render() {
       
        return (
            <div>
            <button onClick = {this.handleCareer} type="Button" className='bobouttonIT' value = "IT">IT</button>
            <button onClick = {this.handleCareer} type="button" className='bobouttonMarketing' value = "Marketing" >Marketing</button>
            <button onClick = {this.handleCareer} type="button" className='bobouttonHR' value = "HR">HR</button>
            <button onClick = {this.handleCareer} type="button" className='bobouttonfinance' value = "Finance">Finance</button>
            {/* <button onClick = {this.handleCareer} type="button" value=""/>
            <button onClick = {this.handleCareer} type="button" value=""/>
            <button onClick = {this.handleCareer} type="button" value=""/>
            <button onClick = {this.handleCareer} type="button" value=""/> */}
            <ReactAudioPlayer
                        src="bg.mp3"
                        autoPlay
                        volume={0.1}
                        style={{opacity:0}}/>
            </div>
            
        );
      }

}

export default ChooseCareer;