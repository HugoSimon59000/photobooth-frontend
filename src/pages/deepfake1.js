import React from 'react'
import ReactPlayer from 'react-player';
import ReactAudioPlayer from 'react-audio-player'; 
import {Input, Button, Dropdown, Modal} from '@kerberos-io/ui';

class Deepfake1 extends React.Component {
    constructor(props) {
        super(props)

    }
    handlePage  = () => {
        this.props.pageHandler()
    }
    chooseText = () => {
        // Given a career, set the right text 
        console.log(this.props.Career)
        if(this.props.Career === 'Finance'){
       
            return(<div className='deepfake-text'>Job : Data Detective <br/><br/>
            Description : From 2027 on, you become a DATA DETECTIVE. You are constantly looking and combining data for new insights. And AI is doing the boring job of delivering you the data like you need them. AI even tries to make your presentations. You and AI are a good team, that’s for sure! 
        
            
            </div>
            )
           
        }
        else if(this.props.Career === 'HR'){
           
            return(<div className='deepfake-text'>Job : work/home HR manager <br/><br/>
            Description : . You made sure that every person in the company could find the ideal balance between private and professional life, working from home, on the road and at the office.
        
             </div>
        )}
        else if(this.props.Career === 'IT'){
            
            return(<div className='deepfake-text'> Job : Digital twin Engineer <br/> <br/>
            Your story : Curious when you switched careers? In 2027, you had to test the safety measures for a giant windmill in Costa Rica. You started by making a digital representation of the mill and then you secured it. People were so impressed by your work, that they asked to do the same for all windmills and hydro-electric installations in South America. And you became a successful DIGITAL TWIN ENGINEER and at the same time living la vida loca in Costa Rica. But I see time is running short, and in 2035 it’s time now to go surfing while the waves are still high here at the Playa Grande. 
        
            </div>)
        }

        else if(this.props.Career === 'Marketing'){
            
            return(<div className='deepfake-text'>Job : Metaverse Experience Manager 
            Description : As an experience manager, you had to listen carefully to what customers want. Thx to Artificial Intelligence, you captured moods, insights and sentiments of people like no one else. 
           
             </div>
        )}
        else {
            return("Forgot to choose a career path")
        }
      
    }
    render(){
        return (
            <div className='question-section'>
                <div className="rendering">
                <div className='badge-participant'>
                    <div className='center'>
                        <h1 className='pilotName'>Check your 2035 job !</h1>
                    </div>
                <div className='flex-grid'>
                    <div class='left-thousand'>
                        <img  src={`data:image/png;base64,${this.props.photo1}`}  style={{height:'400px'}}/>
                    </div>
                    <div class='right-thousand'>
                        {this.chooseText()}

                    </div>
                    </div>
                <div className={"center center-buttons"}>
                    <Button label='Continue the experience' type={"default"} onClick={this.handlePage}/>
                    <ReactAudioPlayer
                        src="bg.mp3"
                        autoPlay
                        volume={0.1}
                        style={{opacity:0}}/>
                    
                </div>
             </div>
            </div>
        </div>
        )
    }
}
export default Deepfake1
