import React from 'react'
import ReactPlayer from 'react-player';
import {Input, Button, Dropdown, Modal} from '@kerberos-io/ui';

class Outro extends React.Component {
    constructor(props) {
        super(props)

    }
    handlePage  = () => {
        this.props.pageHandler()
    }
    render(){
        return (
            <div className='question-section'>
                <div className="rendering">
                <ReactPlayer
                    id = 'introVideo'
                    url=""
                    width="100%"
                    height="1366px"
                    playing
                />
                <div className={"center center-buttons"}>
                    <Button label='next page' onClick={this.handlePage}/>
                </div>
            </div>
        </div>
        )
    }
}
export default Outro
