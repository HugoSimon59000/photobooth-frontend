import React from 'react'
import ReactPlayer from 'react-player';
import {Input, Button, Dropdown, Modal} from '@kerberos-io/ui';
import ReactAudioPlayer from 'react-audio-player'; 
// List of Pilot, you can add new Pilots when you want but the file need to have the same name as in the list and jpg format
let PilotList = ['Elon Musk', 'Chewbacca', 'Spock']
class HallOfFame extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            n : 0
        }

    }
    
    componentDidMount() {

    }
    changePilot = async () => {
        // Function that change the pilot and go to the next page at the nend 
        await this.setState({n: this.state.n+1})
        console.log(this.state.n)
        if(this.state.n == PilotList.length){
            this.handlePage()
        }
    }
    handlePage  = () => {
        this.props.pageHandler()
    }
    render(){
        return (
            <div className='question-section badge-participant'>
                <div className="rendering">
                    <div className='center' style={{width:'70% !important'}}>
                    <h2 className='pilotName'> While we process all data and produce your ID badge, have a look at our hall of fame of SAP Job Time Machine pilots. </h2>
                    </div>
                    <div className='center-buttons'>
                <img id = "participantPhoto" src = {PilotList[this.state.n]+".jpg"} style={{width:'50%'}}  />
                    </div>
                    <div className='center' style={{width:'70% !important'}}>
                    <h2 className = 'pilotName' >    Pilot {this.state.n + 1} : {PilotList[this.state.n]}    </h2>
                    </div>
                <div className={"center center-buttons"}>
                    <Button label='Next Pilot' type={"default"} onClick={this.changePilot}/>
                    <ReactAudioPlayer
                        src="bg.mp3"
                        autoPlay
                        volume={0.1}
                        style={{opacity:0}}/>
                </div>
            </div>
        </div>
        )
    }
}
export default HallOfFame
