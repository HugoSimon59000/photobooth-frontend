import React from 'react'
import ReactPlayer from 'react-player';
import {Input, Button, Dropdown, Modal} from '@kerberos-io/ui';

class Transition2 extends React.Component {
    constructor(props) {
        super(props)

    }
    componentDidMount() {
        // DidMount to go directly into the next page after the end of the video 
        setTimeout(() => {
            this.props.restartApp();
        }, 30000);
        var video=document.getElementById("videolaunch");
        video.muted = !video.muted;
    }
 
    handlePage  = () => {
        this.props.pageHandler()
    }
    render(){
        return (
            <div className='question-section'>
                <div className="rendering">
                <video id={"videolaunch"} muted ref="vidIntro" autoPlay>
                     <source src="04_JUMP_2022.mp4" type="video/mp4"/>
                     </video>  
            </div>
        </div>
        )
    }
}
export default Transition2
