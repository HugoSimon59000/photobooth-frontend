import React from 'react'
import ReactPlayer from 'react-player';
import ReactAudioPlayer from 'react-audio-player'; 
import {Input, Button, Dropdown, Modal} from '@kerberos-io/ui';

class Deepfake2 extends React.Component {
    constructor(props) {
        super(props)

    }
    handlePage  = () => {
        this.props.pageHandler()
    }
    chooseText = () => {
        // Given a career, choose the adapted text 
        if(this.props.Career === 'Finance'){
            setTimeout(() => {
                this.props.pageHandler();
            }, 41000);
            return(<div className='deepfake-text2'>Hi there! You look like a boss in 2050! You can be proud of yourself! You have become the CFO of Inbev, the famous worldwide brewery that originated from Belgium and is using cool SAP technology. After you became a data detective, you soon were the person that everybody needed for insights and decision-making. So when the job of CFO became vacant, they thought of you. So continue your journey and a small tip, stay connected with the digital trends and technologies! <br/><br/> Hang on, we’re going back to the present

                </div>
            )
        }
        else if(this.props.Career === 'HR'){
            
            return(<div className='deepfake-text2'>
            I’ve got news for you. First, you are still looking good in 2050, congratulations, you must have been working out a lot. Second, you are nominated for the Belgian HR Director of the year. And you really deserve it. You have always been a pioneer in using technology in your HR department. By automating all administration and using artificial intelligence for advise, your HR staff excelled in guiding employees in their career. 
            Pretty impressive, right? Interested in pursuing your own career where digital technologies help you to make a difference in this world? I’ll send you some information from your future self to help you out, don’t worry. Bye for now!
                </div>
        )}
        else if(this.props.Career === 'IT'){
            
            return(<div className='deepfake-text2'> Digital hacker at 18, digital twin engineer at 35 and what have you become in 2050? Let me tell you. As the CIO of La Paloma Renewable Energies South America in 2050, you have become one of the most respected IT visionaries on the continent. There’s not one single cool technological project in the industry or it bears your name. Pretty impressive, right? You want to unlock the IT person that’s inside of you? I’ll send you some information from your future self to help you out, don’t worry. Bye for now!<br/><br/> Hang on, we’re going back to the present
                 
                 </div>)
        }

        else if(this.props.Career === 'Marketing'){
           
            return(<div className='deepfake-text2'>Hi interplanetary CMO Director of Coca Cola. If there’s one brand on Earth and on Mars that is about experiences, it’s Coke. Who brought you here in 2050 as marketing director of one of the most powerful brands in the solar system? Your skills combined with your willingness to embrace new technologies. As a mood and empathy manager, you were the ears of your company. You felt every heartbeat, every swing of sentiment. People were inspired by your marketing approach and the company made you in 2045 head of marketing. But I think we have to leave you, because you’re about to leave for the moon to attend the lunar edition of Tomorrowland, sponsored by Coke. Bye for now! <br/><br/>Hang on, we’re going back to the present
                    
                    </div>
        )}
        else {
            return("Forgot to choose a career path")
        }
      
    }
    render(){
        return (
            <div className='question-section'>
            <div className="rendering">
            <div className='badge-participant'>
                <div className='center'>
                    <h1 className='pilotName'>Check your 2050 job !</h1>
                </div>
            <div className='flex-grid'>
                <div class='left-thousand'>
                    <img  src={`data:image/png;base64,${this.props.photo2}`}  style={{height:'400px'}}/>
                </div>
                <div class='right-thousand'>
                    {this.chooseText()}

                </div>
                </div>
            <div className={"center center-buttons"}>
                <Button label='Continue the experience' type={"default"} onClick={this.handlePage}/>
                <ReactAudioPlayer
                        src="bg.mp3"
                        autoPlay
                        volume={0.1}
                        style={{opacity:0}}/>
            </div>
         </div>
        </div>
    </div>

        )
    }
}
export default Deepfake2
