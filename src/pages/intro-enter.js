import React from 'react';
import {Input, Button, Dropdown, Modal} from '@kerberos-io/ui';
import ReactAudioPlayer from 'react-audio-player'; 



class EnterParticipant extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            name: "",
            job: "sales",
            advice: "BeCode",
            gender: [""],
            email : "",
            isAvailable : "disabled",
            isEmptyName : false,
            isEmptyEmail : false,
            isEmptyGender : false,

            isCheckCB1 : false,
            isCheckCB2 : false,
            init : false


        }
        this.handlePage = this.handlePage.bind(this);
        this.handleName = this.handleName.bind(this);
        this.handleGender = this.handleGender.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handleCB1 = this.handleCB1.bind(this);
        this.handleCB2 = this.handleCB2.bind(this);


    }
    handleName = (event) => {
        // Get Username
        this.setState({name: event.target.value});
    }
    handleEmail = (event) => {
        // Get user's mail
        this.setState({email: event.target.value});
    }
    checkEmail = (e) => {
        // Check email form to really get an email address (Can be improved)
        if(e.target.value.includes('@') && e.target.value != ""){
            this.setState({isEmptyEmail : false})
        }
        else if(!e.target.value.includes('@') && e.target.value != "") {
            this.setState({isEmptyEmail : true})

        }
        else {
            this.setState({isEmptyEmail : true})

        }
    }
    checkName = (e) => {
        // Check that name is not empty 
        if(e.target.value != ""){
            this.setState({isEmptyName : false})
        }
        else {
            this.setState({isEmptyName : true})
        }
        
    }  
    handleCB1 = () => {
        // Get checkbox's status 
        if(this.state.isCheckCB1){
            this.setState({isCheckCB1 : false})
        }
        else {
            this.setState({isCheckCB1 : true}) 
        }
    }
    handleCB2 = () => {
        // Get Checkbox's status
        if(this.state.isCheckCB2){
            this.setState({isCheckCB2 : false})
            this.setState({init : true})
        }
        else {
            this.setState({isCheckCB2 : true}) 
            this.setState({init : true})
        }
    }
    
        
    
    handleGender = (value) => {
        // Get user's gender
        this.setState({gender: value});
        this.setState({isEmptyGender : false})

    }
    handlePage = () => {
        // check if everything is right in the form
        if(!this.state.name){
            this.setState({isEmptyName : true})
        }
        if(!this.state.email){
            this.setState({isEmptyEmail : true})
        }
        if (this.state.gender[0] == ""){
            this.setState({isEmptyGender : true})
        }
        if(!this.state.isCheckCB2){
            this.setState({isCheckCB2 : false})
            this.setState({init : true})
        }
        
        if(this.state.name != "" && this.state.email != "" && this.state.email.includes('@') && this.state.gender[0] != "" && this.state.isCheckCB2){
            this.props.dataHandler(
                this.state.name,
                this.state.gender
            )
            if(this.state.isCheckCB1){
                this.props.emailHandler(this.state.email)
            }
            this.props.pageHandler()
        }
        
    }

    
    render(){
        this.defaultProps = {
            emailAgreement: false,
            photoAgreement: false
          }
        return (
            <div className='question-section enter-participant'>
                <div className = "rendering enter-form" >
                    <form>
                        <label className='labelForm'>
                            First Name :
                            <Input value={this.state.name} onBlur={this.checkName} onChange={this.handleName} placeholder='Type...'/>
                        </label>
                        {this.state.isEmptyName && <div className='alertForm'>Please complete this field</div>}

                        <label className='labelForm'>
                            Email:
                            <Input value={this.state.email} onBlur={this.checkEmail} onChange={this.handleEmail} placeholder='Type...'  type="email"/>
                        </label>
                        {this.state.isEmptyEmail && <div className='alertForm'>Please complete this field with a correct email address</div>}

                        <label className='labelForm'>
                            Your gender:
                            <Dropdown isRadio={true} selected={this.state.gender} placeholder="Select a gender" items={[{
                                label:"Male",
                                value:"Male"
                            },{
                                label:"Female",
                                value:"Female"
                            },{
                                label:"Other",
                                value:"Other"
                            }]} onChange={this.handleGender} onClick={this.checkGender}></Dropdown>
                        </label>
                        {this.state.isEmptyGender && <div className='alertForm'>Please select a gender</div>}
                        <div>
                        <label style={{color:"white"}}>
                            <input type='checkbox' id='emailSender' onChange={this.handleCB1}/>
                            Check if you agree to receive mails from SAP about Jobs, Courses.
                        </label>
                        </div>
                        <label style={{color:"white"}}>
                            <input type='checkbox' id='photoAgreement'  onChange={this.handleCB2}/>
                            Check if you agree to take a picture of you. This picture will not be stored in any database
                            {!this.state.isCheckCB2 && this.state.init && <div className='alertForm'>Please check to continue the experience</div>}

                        </label>

                    </form>
                    <div className={"buttons left"}>
                        <Button type={"default"} icon={"arrowright"}
                                label={"Press to enter the Photo Booth"}
                                onClick={this.handlePage}></Button>
                        
                    </div>
                    <br/>
                </div>
            </div>
        )
    }
}

export default EnterParticipant
