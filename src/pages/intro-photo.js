import React from 'react';

import WebcamCapture from "./tools/webcam.js";

class TakePhoto extends React.Component {

    constructor(props) {
        super(props)
        this.getPhoto = this.getPhoto.bind(this)

    }
    getPhoto =  (img) => {
        // Get user's photo
        this.props.dataHandler(img)
    }


    render(){
        // Simple rendering of the webcam componenent
        return (
            <div className='question-section'>
                <WebcamCapture
                    pageHandler = {this.props.pageHandler}
                    participant = {this.props.participant}
                    gender = {this.props.gender}
                    api = {this.props.api}
                    dataHandler = {this.getPhoto}
                />
            </div>
        )
    }
}

export default TakePhoto;
