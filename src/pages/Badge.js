import React from 'react'
import ReactPlayer from 'react-player';
import {Input, Button, Dropdown, Modal} from '@kerberos-io/ui';
import ReactAudioPlayer from 'react-audio-player'; 


class Badge extends React.Component {
    constructor(props) {
        super(props)

    }
    handlePage  = () => {
        this.props.pageHandler()
    }

    render(){
        return (
            <div className='question-section badge-participant'>
                <div className="rendering">
                    <div className='center' style={{width:'70% !important'}}>
                    <h2 className='pilotName'> Everything seems to work out fine. Are you ready for take off? Then push the start button on the screen</h2>
                    </div>
                    <div className='center-buttons'>
                <img id = "participantPhoto" src = {this.props.photo} style={{width:'50%'}}/>
                    </div>
                <div className={"center center-buttons"}>
                    <Button label='Jump into the future' type={"default"} onClick={this.handlePage}/>
                    <ReactAudioPlayer
                        src="bg.mp3"
                        autoPlay
                        volume={0.1}
                        style={{opacity:0}}/>
                   
                </div>
            </div>
        </div>
        )
    }
}
export default Badge
