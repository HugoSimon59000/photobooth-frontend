import React from 'react'
import ReactPlayer from 'react-player';
import {Input, Button, Dropdown, Modal} from '@kerberos-io/ui';
import axios from 'axios';

class Video2 extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showLoader: false
        }
        this.checkaging = this.checkaging.bind(this)
        // TODO for testing!
        //setTimeout(this.props.pageHandler, 25000);
    }
    
    componentDidMount() {
        // DidMount to go directly into the next page after the end of the video but also launch deepfake computation
        this.checkaging()
        setTimeout(() => {
            this.props.pageHandler();
        }, 30000);
    }
        
        
    checkaging = () => {
        // This function send a request to AI Core to launch the calculation of the Deep Fake 
        console.log("checking...")
        console.log(this.props.gender)
        let substring = "data:image/png;base64,"
        let data_img = this.props.image.slice(substring.length)
        let datajson = JSON.stringify({
            "img":data_img,
            "gender": this.props.gender[0]
        })
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: datajson,
            redirect: 'follow'
          };
          
          fetch(this.props.api + "/aicore/aging", requestOptions)
            .then(response => response.json())
            .then(result =>  {
                this.props.dfHandler(result['response'], result['response2'])
                
                
            })
            .catch(error => console.log('error', error));

    }
    
    render(){
        return (
            <div className='question-section'>
                <div className = "rendering">
                    <video id="questionsVideo" className="introVideo" width={"100%"} autoPlay>
                        <source src="01_LAUNCH_SEQUENCE_SHORT.mp4" type="video/mp4"/>
                    </video>
                </div>
            </div>
        )
    }
    
}
export default Video2
