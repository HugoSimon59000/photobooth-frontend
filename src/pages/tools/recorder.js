import React from 'react';
import axios from 'axios';

import {ReactMic} from 'react-mic';
import Dictaphone from './speech-recognition.js'

export class Recorder extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            started: false,
            record: false,
            recordAfter: props.recorderWait,
            voiceHandle: props.voiceHandle,
            voicePrompt: " ",
        }
        this.onStop = this.onStop.bind(this)
    }

    startRecording = () => {
        this.setState({ started: true })
        this.setState({voicePrompt: "Say: " + this.props.voiceCommand})
        this.setState({ record: true })
    }

    stopRecording = () => {
        this.setState({ record: false })
        this.props.pageHandler()
    }

    onData(recordedBlob) {
    }

    onStop(recordedBlob) {
        if (this.props.recordingNumber){
            const filename = "voice_" + this.props.recordingNumber + ".wav"
            const data = new FormData()
            data.append("participant", this.props.participant)
            data.append("file", recordedBlob.blob, filename)
            data.append("number", this.props.recordingNumber)
            // axios.post(this.props.api + "/voice", data)
        }

    }

    render() {
        if (!this.state.started){
            setTimeout(() => {this.startRecording()}, this.state.recordAfter)
        }
        return (
            <div>
            { this.state.started ? (
                <div>
                         <Dictaphone
                            pageHandler={this.stopRecording}
                            voiceHandler={this.props.voiceHandle}
                            voiceCommand={this.props.voiceCommand}
                            restartCommand={this.props.restartCommand}
                            restartHandler={this.props.restartHandler}
                            leftHandle={this.props.leftHandle}
                            rightHandle={this.props.rightHandle}
                        />
                        <ReactMic
                            record={this.state.record}
                            className="sound-wave"
                            onStop={this.onStop}
                            onData={this.onData}
                            strokeColor="#F0AB00"
                            backgroundColor="#000000"
                            mimeType="audio/wav"
                        />
                    </div>
                ) : ( <div> </div>)
            }
            </div>
        )
    }
}