import React, {useState}  from 'react';

import {Recorder} from './recorder.js'
import Webcam from "react-webcam";
import axios from 'axios';
import { Button } from "@kerberos-io/ui"

const videoConstraints = {
  width: {exact: 500},
  height: {exact: 500},
  facingMode: "user"
};

const WebcamCapture = ({pageHandler, participant, gender, api, dataHandler}) => {

    const [image,setImage] = useState(null)
    const [flash, setFlash] = useState(false)
    const [timer, setTimer] = useState(5)
    const [pressed, setPressed] = useState(false)
    const webcamRef = React.useRef(null)

    const capture = React.useCallback(() => {
        if(webcamRef.current) {
            const imageSrc = webcamRef.current.getScreenshot()
            setImage(imageSrc)
        }
    })

    const start = () => {
        for (let i = 0; i < timer + 2; i++) {
            setTimeout(() => {
                if (!image){
                    if (timer < i) {capture()}
                    else if (timer == i) {setTimer(0); setFlash(true)}
                    else {setTimer(timer - i)}
                }
            }, 1000 * i)
        }
    }
    const handlePhoto = () => {
        dataHandler(image)
    }


    return (
        <>
            {image ?
                <>
                    <h1 className={"center"}  style={{color:'white'}}>You are looking great!</h1>

                    <div className={"center center-buttons"}>
                        <Button label={"I like this photo"} type={"default"} onClick={() => {
                            if(pressed===false) {
                                console.log("pressed")
                                setPressed(true);
                                fetch(image)
                                    .then((res) => res.blob())
                                    .then((blob) => {
                                        // Maybe useless
                                        const data = new FormData()
                                        data.append("participant", participant)
                                        data.append("gender", gender)
                                        const file = new File([blob], 'source.png', blob)
                                        console.log(file)
                                        data.append("file", file)
                                        //Get the photo and go to the next page 
                                        handlePhoto()
                                        pageHandler()

                                    })
                            }}
                        }></Button>

                        <Button label={"Retake photo"} onClick={() => {
                            if(pressed===false) {
                                setTimer(6)
                                setImage(null)
                                setFlash(false)
                            }
                        }}></Button>

                    </div>

                    <img id = "participantPhoto" src = {image} />

                </> :
                <>

                    <h1 className={"center"} style={{color:'white'}}>We are taking your picture in {timer}s</h1>
                    <h2 className={"center"} style={{color:'white'}}> Please remove your glasses for the photo</h2>
                    <div className={"background-black"}>
                        { flash && <div id="rectangle"></div> }
                        <Webcam
                            audio={false}
                            mirrored={true}
                            ref={webcamRef}
                            screenshotFormat="image/png"
                            videoConstraints={videoConstraints}
                            onUserMedia={() => start()}
                        />
                    </div>
                </>
            }
        </>
    );
}

export default WebcamCapture
