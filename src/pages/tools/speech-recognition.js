import React from 'react'
import SpeechRecognition, { useSpeechRecognition } from 'react-speech-recognition';

const Dictaphone = ({
    pageHandler,
    voiceHandler,
    voiceCommand,
    restartCommand,
    restartHandler,
    quizHandler,
    quizQuestions,
    countryHandler,
    countryHandlerDefault,
    leftHandle,
    rightHandle
}) => {
    const commands = []
    if (voiceHandler==="continue"){
        commands.push({
            command: voiceCommand,
            callback: () => {
                SpeechRecognition.stopListening()
                pageHandler()
            },
            isFuzzyMatch: true,
            fuzzyMatchingThreshold: 0.5
        })
    }
    if (voiceHandler==="photo"){
        commands.push({
            command: voiceCommand,
            callback: () => {
                SpeechRecognition.stopListening()
                pageHandler()
            },
            isFuzzyMatch: true,
            fuzzyMatchingThreshold: 0.5
        })
        commands.push({
            command: restartCommand,
            callback: () => {
                SpeechRecognition.stopListening()
                restartHandler()
            },
            isFuzzyMatch: true,
            fuzzyMatchingThreshold: 0.5
        })
    }
    if (voiceHandler==="personality"){
        commands.push({
            command: "left",
            callback: (command) => {
                leftHandle()
            },
            isFuzzyMatch: true,
            fuzzyMatchingThreshold: 0.8
        })
        commands.push({
            command: "right",
            callback: (command) => {
                rightHandle();
            },
            isFuzzyMatch: true,
            fuzzyMatchingThreshold: 0.8
        })
    }

    useSpeechRecognition({ commands })
    SpeechRecognition.startListening({ continuous: true, language: 'en-US'})

    if (voiceHandler==="continue"){
        return(
            <div>
                <button onClick={pageHandler}> Say: "{voiceCommand}" to continue </button>
                <br/>
            </div>
        )
    }
    if (voiceHandler==="photo"){
        return(
            <div>
                <button onClick={pageHandler}> Say: "{voiceCommand}" to continue </button>
                <br/>
                <button onClick={restartHandler}> Say: "{restartCommand}" to retake </button>
                <br/>
            </div>
        )
    }

    if (voiceHandler==="personality"){
        return(
            <div>
                <br/>
            </div>
        )
    }
}

export default Dictaphone